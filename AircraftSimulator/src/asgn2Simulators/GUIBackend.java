/**
 * 
 */
package asgn2Simulators;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.jfree.data.xy.XYDataset;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import asgn2Aircraft.AircraftException;
import asgn2Passengers.PassengerException;

/**
 * @author saujanthapa
 *
 */
public class GUIBackend {

	/**
	 * This class is helper class for gui. This is the heart of gui which does all the backend calculation
	 * 
	 * //fields for data
	 */
	
	private Simulator sim;
	private int seed;
	private int maxQueueSize;
	private double cancelProb;
	private double meanDailyBookings;
	private double sdDailyBookings;
	private double firstProb;
	private double businessProb;
	private double premiumProb;
	private double economyProb;
	private String output;
	private XYSeries queueset;
	private XYSeries refusedset;
	private XYSeries firstset;
	private XYSeries businessset;
	private XYSeries premiumset;
	private XYSeries economyset;
	private XYSeries totalset;
	private XYSeries seatavailableset;
	private XYSeriesCollection endsimulationcollection;
	private XYSeriesCollection simulationcollection;
	private int oldfirst;
	private int oldbusiness;
	private int oldpremium;
	private int oldeconomy;
	private int oldtotal;
	private int oldseat;
	private int newtotal;
	//private SimulationRunner s;
	
	/**
	 * Constructor which initializes all the fields
	 * 
	 * @param seed <code>int</code> random number generator seed
	 * @param queue <code>int</code> holding max permissible queue size
	 * @param mean <code>double</code> holding the mean of the Normal Distribution of daily bookings
	 * @param cancel <code>double</code> holding the probability of cancellation from confirmedSeat
	 * @param first <code>double</code> holding the probability that current booking will be First Class
	 * @param business <code>double</code> holding the probability that current booking will be Business Class 
	 * @param premium <code>double</code> holding the probability that current booking will be Premium Economy
	 * @param economy <code>double</code> holding the probability that current booking will be Economy Class
	 * @throws SimulationException if one or more probabilities are invalid, or if meanDailyBookings < 0 or sdDailyBookings < 0
	 */
	public GUIBackend(int seed, int queue, double mean,double sd, double cancel, double first, double business, double premium, double economy) throws SimulationException {
		this.seed = seed;
		this.maxQueueSize = queue;
		this.meanDailyBookings = mean;
		this.cancelProb = cancel;
		this.firstProb = first;
		this.businessProb = business;
		this.premiumProb = premium;
		this.economyProb = economy;
		this.sdDailyBookings = sd;
		this.sim = new Simulator(this.seed, this.maxQueueSize, this.meanDailyBookings, this.sdDailyBookings,
				this.firstProb, this.businessProb, this.premiumProb, this.economyProb, this.cancelProb);
		this.queueset = new XYSeries("Queue");
		this.refusedset = new XYSeries("Refused");
		this.firstset = new XYSeries("First");
		this.businessset = new XYSeries("Business");
		this.premiumset = new XYSeries("Premium");
		this.economyset = new XYSeries("Economy");
		this.totalset = new XYSeries("Total");
		this.seatavailableset = new XYSeries("Available seat");
		this.endsimulationcollection = new XYSeriesCollection ();
		this.simulationcollection = new XYSeriesCollection();
		this.oldfirst=0;
		this.oldbusiness=0;
		this.oldpremium=0;
		this.oldeconomy=0;
		this.oldtotal=0;
		this.oldseat=0;
		this.newtotal=0;
		this.output="";
		
	}
	
	/**
	 * This is the function which runs the simulation when button on gui is pressed
	 * 
	 * @throws AircraftException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws PassengerException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws SimulationException See methods from {@link asgn2Simulators.Simulator}
	 * @throws IOException 
	 */
	public void processFlights() throws AircraftException, SimulationException, PassengerException, IOException{
		//s.runSimulation();
		this.sim.createSchedule();
		appendStringLine(getSystemTime()+ ": Start of Simulation\n");
		appendStringLine(this.sim.toString()+"\n");
		appendStringLine(this.sim.getFlights(Constants.FIRST_FLIGHT).initialState());
		//Main simulation loop 
				for (int time=0; time<=Constants.DURATION; time++) {
					this.sim.resetStatus(time); 
					this.sim.rebookCancelledPassengers(time); 
					this.sim.generateAndHandleBookings(time);
					this.sim.processNewCancellations(time);
					if (time >= Constants.FIRST_FLIGHT) {
						this.sim.processUpgrades(time);
						this.sim.processQueue(time);
						this.sim.flyPassengers(time);
						this.sim.updateTotalCounts(time); 
						if (Log.SAVE_STATUS){
							Flights f=this.sim.getFlights(time);
							appendStringLine(f.getStatus(time));
						}
					} 
					else {
						this.sim.processQueue(time);
					}
					if (Log.SAVE_STATUS){
						appendStringLine(this.sim.getStatus(time));
					}
						appendStringLine(this.sim.getSummary(time,time >= Constants.FIRST_FLIGHT));

						//Flights f=this.sim.getFlights(time);
					addDataSimulation(time);	
					//storing the number of queued passenger in dataset 
					this.queueset.add(time, this.sim.numInQueue());
					//storing the number of refued passenger in dataset 
					this.refusedset.add(time, this.sim.numRefused());
				}
				this.sim.finaliseQueuedAndCancelledPassengers(Constants.DURATION);
				if (Log.SAVE_STATUS){
				appendStringLine(this.sim.getStatus(Constants.DURATION));
			}
			appendStringLine("\n" + getSystemTime() + ": End of Simulation\n");
			appendStringLine(this.sim.finalState());
			this.endsimulationcollection.addSeries(this.queueset);
			this.endsimulationcollection.addSeries(this.refusedset);
			
			this.simulationcollection.addSeries(this.firstset);
			this.simulationcollection.addSeries(this.businessset);
			this.simulationcollection.addSeries(this.premiumset);
			this.simulationcollection.addSeries(this.economyset);
			this.simulationcollection.addSeries(this.totalset);
			this.simulationcollection.addSeries(this.seatavailableset);
			saveToFile();
	}

	/**
	 * This function stores the number of passengers of respective type at certain time for chartting 
	 * @param t <cod>int</code>current time
	 */
	private void addDataSimulation(int t){
		this.firstset.add(t, this.sim.getTotalFirst()-this.oldfirst);
		this.oldfirst=this.sim.getTotalFirst();
		this.businessset.add(t,this.sim.getTotalBusiness()-this.oldbusiness);
		this.oldbusiness=this.sim.getTotalBusiness();
		this.premiumset.add(t,this.sim.getTotalPremium()-this.oldpremium);
		this.oldpremium=this.sim.getTotalPremium();
		this.economyset.add(t,this.sim.getTotalEconomy()-this.oldeconomy);
		this.oldeconomy=this.sim.getTotalEconomy();
		this.newtotal=this.sim.getTotalFirst()+this.sim.getTotalBusiness()+this.sim.getTotalPremium()+
				this.sim.getTotalEconomy();
		this.totalset.add(t,this.newtotal-this.oldtotal);
		this.oldtotal=this.sim.getTotalFirst()+this.sim.getTotalBusiness()+this.sim.getTotalPremium()+
				this.sim.getTotalEconomy();
		this.seatavailableset.add(t,this.sim.getTotalEmpty()-this.oldseat);
		this.oldseat=this.sim.getTotalEmpty();
	}
	
	private void saveToFile() throws IOException{
		File logFile = new File(getSystemTime());
		FileWriter fw = new FileWriter(logFile);
		fw.write(this.output);
		fw.close();
	}
	
	/**
	 * This function returns the dataset for plotting after simulation is completed
	 * @return <code>XYSeriesCollection</code> contains the queue and refuse sets
	 */
	public XYDataset getQueueAndRefusedDataSet(){
		return this.endsimulationcollection;
	}
	
	/**
	 * This function returns the dataset for plotting the flight simulation
	 * @return <code>XYSeriesCollection</code> contains first,business,premium,economy, total and seat available sets
	 */
	public XYDataset getSimulationDataSet() {
	
		return this.simulationcollection;
	}
	
	private void appendStringLine(String newstr){
		this.output +=newstr;
	}

	public String getResults(){
		return this.output;
	}

	private String getSystemTime(){
		String timeLog = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
		return timeLog;
	}
	
}
