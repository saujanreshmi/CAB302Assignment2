/**
 * 
 * This file is part of the AircraftSimulator Project, written as 
 * part of the assessment for CAB302, semester 1, 2016. 
 * 
 */
package asgn2Simulators;

import java.awt.BorderLayout;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.xy.XYDataset;
import asgn2Aircraft.AircraftException;
import asgn2Passengers.PassengerException;

/**
 * @author n8962136
 *
 */
public class GUISimulator extends JFrame implements ActionListener{
	
	private static final long serialVersionUID = 1L;
	public static final int WIDTH = 850;
	public static final int HEIGHT = 680;
	
	private JButton btnRunSimulator;
	private JButton btnShowChart;
	private JButton btnReset;
	private JPanel panelSimulationResults;
	private JTextArea textAreaSimulationResults;
	private JTabbedPane tabbedPane;
	private JTextField textFieldRNGSeed;
	private JTextField textFieldDailyMean;
	private JTextField textFieldQueueSize;
	private JTextField textFieldCancellation;
	private JTextField textFieldFirst;
	private JTextField textFieldBusiness;
	private JTextField textFieldPremium;
	private JTextField textFieldEconomy;
	private ChartPanel chartpanel;
	private JFreeChart chart;
	private GUIBackend gb;
	
	private static int seed;
	private static int queue;
	private static double mean;
	private static double cancel;
	private static double first;
	private static double business;
	private static double premium;
	private static double economy;
	private static double sd;
	
	/**
	 * Constructor 
	 * 
	 * @param arg0 <code>String</code> title of the window
	 * @throws HeadlessException if <code>title</code> is missing
	 * Simple constructor for the GUISimulator class that sets up each component as
	 * well as event handlers and default simulation values
	 */
	public GUISimulator(String arg0) throws HeadlessException {
		super(arg0);
		initialise();
		setUpDefaultValues();
	}	

	/**
	 * This function sets all the fields in default values
	 */
	private void setUpDefaultValues(){
		textFieldRNGSeed.setText(""+Constants.DEFAULT_SEED);
		textFieldDailyMean.setText(""+Constants.DEFAULT_DAILY_BOOKING_MEAN);
		textFieldQueueSize.setText(""+Constants.DEFAULT_MAX_QUEUE_SIZE);
		textFieldCancellation.setText(""+Constants.DEFAULT_CANCELLATION_PROB);
		textFieldFirst.setText(""+Constants.DEFAULT_FIRST_PROB);
		textFieldBusiness.setText(""+Constants.DEFAULT_BUSINESS_PROB);
		textFieldPremium.setText(""+Constants.DEFAULT_PREMIUM_PROB);
		textFieldEconomy.setText(""+Constants.DEFAULT_ECONOMY_PROB);
	}
	
	/**
	 * Main Program including a run() function that instantiates the GUI
	 * @param arg0 - Window title of the simulator 
	 * @throws HeadlessException if environment does not support display, keyboard or mouse
	 */
	public static Simulator guiFlightSimulator(String[] args) throws SimulationException {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUISimulator window = new GUISimulator("Flight Simulator");
					window.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		if (args!=null){
		seed = Integer.parseInt(args[0]);
		queue = Integer.parseInt(args[1]);
		mean = Double.parseDouble(args[2]);
		sd = Double.parseDouble(args[3]);
		first = Double.parseDouble(args[4]);
		business = Double.parseDouble(args[5]);
		premium = Double.parseDouble(args[6]);
		economy = Double.parseDouble(args[7]);
		cancel = Double.parseDouble(args[8]);
		}
		return new Simulator();
	}
	
	/**
	 * This is GUI component initialiser(default) function 
	 */
	private void initialise() {
		//Initialise environment
		setBounds(100, 100, 654, 448);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);

		//Buttons
		btnRunSimulator = new JButton("Run Simulator");
		btnRunSimulator.setBounds(555, 528, 156, 37);
		getContentPane().add(btnRunSimulator, BorderLayout.SOUTH);
		btnRunSimulator.addActionListener(this);
		
		btnShowChart = new JButton("Show Chart");
		btnShowChart.setBounds(555, 564, 156, 37);
		btnShowChart.setEnabled(false);
		getContentPane().add(btnShowChart);
		btnShowChart.addActionListener(this);
		
		btnReset = new JButton("Reset");
		btnReset.setBounds(555, 600, 156, 37);
		getContentPane().add(btnReset, BorderLayout.SOUTH);
		btnReset.addActionListener(this);
		btnReset.setEnabled(false);
		
		//JPanel to contain chart results
		panelSimulationResults = new JPanel();
		panelSimulationResults.setBounds(30,12,940,467);
		panelSimulationResults.setBackground(Color.WHITE);
		getContentPane().add(btnShowChart);
		
		//Text area that supplies text log results
		textAreaSimulationResults = new JTextArea();
		textAreaSimulationResults.setBackground(Color.WHITE);
		textAreaSimulationResults.setLineWrap(true);
		textAreaSimulationResults.setWrapStyleWord(true);
		textAreaSimulationResults.setMargin( new Insets(5,5,5,5) );
		textAreaSimulationResults.setEditable(false);
		JScrollPane scrollPane = new JScrollPane(textAreaSimulationResults);
		scrollPane.setBounds(30,12,790,467);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		
		chart =  ChartFactory.createXYLineChart(
	            "Empty",      // chart title
	            "Days",                      // x axis label
	            "Passengers",                      // y axis label
	            null,                  // data
	            PlotOrientation.VERTICAL,
	            true,                     // include legend
	            true,                     // tooltips
	            false                     // urls
	        );
		chartpanel = new ChartPanel(chart);
		panelSimulationResults.setLayout(new java.awt.BorderLayout());
		panelSimulationResults.add(chartpanel,BorderLayout.CENTER);
		panelSimulationResults.validate();

		//JTabbedPane that contains the text and chart simulation results in separate tabs
		tabbedPane = new JTabbedPane();
		tabbedPane.setBounds(30,12,790,467);
		tabbedPane.setBackground(Color.WHITE);
		tabbedPane.addTab("Chart Results", panelSimulationResults);
		tabbedPane.addTab("Text Log Results", scrollPane);
		getContentPane().add(tabbedPane);
		
		//Labels
		JLabel lblSimulation = new JLabel("Simulation");
		lblSimulation.setFont(new Font("Dialog", Font.BOLD, 19));
		lblSimulation.setBounds(194, 491, 128, 31);
		getContentPane().add(lblSimulation);

		JLabel lblFareClasses = new JLabel("Fare Classes");
		lblFareClasses.setFont(new Font("Dialog", Font.BOLD, 19));
		lblFareClasses.setBounds(373, 491, 141, 31);
		getContentPane().add(lblFareClasses);

		JLabel lblOperations = new JLabel("Operations");
		lblOperations.setFont(new Font("Dialog", Font.BOLD, 19));
		lblOperations.setBounds(573, 491, 119, 31);
		getContentPane().add(lblOperations);

		JLabel lblRNGSeed = new JLabel("RNG Seed");
		lblRNGSeed.setBounds(132, 535, 70, 15);
		getContentPane().add(lblRNGSeed);

		JLabel lblDailyMean = new JLabel("Daily Mean");
		lblDailyMean.setBounds(132, 559, 78, 15);
		getContentPane().add(lblDailyMean);

		JLabel lblQueueSize = new JLabel("Queue Size");
		lblQueueSize.setBounds(132, 586, 88, 15);
		getContentPane().add(lblQueueSize);

		JLabel lblCancellation = new JLabel("Cancellation");
		lblCancellation.setBounds(132, 613, 88, 15);
		getContentPane().add(lblCancellation);

		JLabel lblFirst = new JLabel("First");
		lblFirst.setBounds(363, 535, 43, 15);
		getContentPane().add(lblFirst);

		JLabel lblBusiness = new JLabel("Business");
		lblBusiness.setBounds(363, 559, 70, 15);
		getContentPane().add(lblBusiness);

		JLabel lblPremium = new JLabel("Premium");
		lblPremium.setBounds(363, 586, 70, 15);
		getContentPane().add(lblPremium);

		JLabel lblEconomy = new JLabel("Economy");
		lblEconomy.setBounds(363, 613, 75, 15);
		getContentPane().add(lblEconomy);

		//Text Fields for simulation parameters
		textFieldRNGSeed = new JTextField();
		textFieldRNGSeed.setBounds(238, 530, 78, 21);
		textFieldRNGSeed.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldRNGSeed);

		textFieldDailyMean = new JTextField();
		textFieldDailyMean.setBounds(238, 559, 78, 21);
		textFieldDailyMean.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldDailyMean);

		textFieldQueueSize = new JTextField();
		textFieldQueueSize.setBounds(238, 586, 78, 21);
		textFieldQueueSize.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldQueueSize);

		textFieldCancellation = new JTextField();
		textFieldCancellation.setBounds(238, 613, 78, 21);
		textFieldCancellation.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldCancellation);

		textFieldFirst = new JTextField();
		textFieldFirst.setBounds(435, 530, 78, 21);
		textFieldFirst.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldFirst);

		textFieldBusiness = new JTextField();
		textFieldBusiness.setBounds(435, 559, 78, 21);
		textFieldBusiness.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldBusiness);

		textFieldPremium = new JTextField();
		textFieldPremium.setBounds(435, 586, 78, 21);
		textFieldPremium.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldPremium);

		textFieldEconomy = new JTextField();
		textFieldEconomy.setBounds(435, 613, 78, 21);
		textFieldEconomy.setHorizontalAlignment(SwingConstants.RIGHT);
		getContentPane().add(textFieldEconomy);
		
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		//Get event source 
		Object src=e.getSource();
		if (src==btnRunSimulator){
			try {
				startSimulation();
			} catch (SimulationException | AircraftException | PassengerException | IOException e1) {
				e1.printStackTrace();
			} 
		}
		else if (src==btnShowChart){
			btnShowChart.setEnabled(false);
			displayChart("Summary",gb.getQueueAndRefusedDataSet());
			btnShowChart.setEnabled(false);
			btnReset.setEnabled(true);
			
		}
		else if (src==btnReset){
			setUpDefaultValues();
			displayChart("Empty",null);
			btnRunSimulator.setEnabled(true);
			btnReset.setEnabled(false);
			//make text boxes editable
			textFieldRNGSeed.setEditable(true);
			textFieldDailyMean.setEditable(true);
			textFieldQueueSize.setEditable(true);
			textFieldCancellation.setEditable(true);
			textFieldFirst.setEditable(true);
			textFieldBusiness.setEditable(true);
			textFieldPremium.setEditable(true);
			textFieldEconomy.setEditable(true);
			textAreaSimulationResults.setText("");
		}
	}
	/**
	 * This function initializes the simulation when 'Run simulator' button is pressed if 
	 * validation rule are not violated. 
	 * 
	 * If any validation rule is not matched this function captures the control.
	 * 
	 * @throws AircraftException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws PassengerException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws SimulationException See methods from {@link asgn2Simulators.Simulator}  
	 * @throws IOException 
	 */
	private void startSimulation() throws SimulationException, AircraftException, PassengerException, IOException{
		//checking for validation
		boolean notValid=true;
			try{
				GUISimulator.seed = Integer.parseInt(textFieldRNGSeed.getText());
				GUISimulator.queue=Integer.parseInt(textFieldQueueSize.getText());
				GUISimulator.mean=Double.parseDouble(textFieldDailyMean.getText());
				GUISimulator.cancel=Double.parseDouble(textFieldCancellation.getText());
				GUISimulator.first=Double.parseDouble(textFieldFirst.getText());
				GUISimulator.business=Double.parseDouble(textFieldBusiness.getText());
				GUISimulator.premium=Double.parseDouble(textFieldPremium.getText());
				GUISimulator.economy=Double.parseDouble(textFieldEconomy.getText());
				GUISimulator.sd = 0.33*GUISimulator.mean;
				notValid=false;
			}
			catch (NumberFormatException e){
				exceptionOccured("Other than number is detected");
				notValid=true;
			}
		
		//checking for validation
			if (GUISimulator.queue<0 || GUISimulator.seed<0 || GUISimulator.mean<0 || GUISimulator.cancel<0 || GUISimulator. first<0 || GUISimulator.business<0 || GUISimulator.premium<0 || GUISimulator.economy<0){
				exceptionOccured("Negative number is detected");
				notValid=true;
			}
			else if (GUISimulator.first+GUISimulator.business+GUISimulator.premium+GUISimulator.economy>1){
				exceptionOccured("Incorrect probability for First, Business, Premium or Economy");
				notValid=true;
			}
			else if (GUISimulator.cancel>1){
				exceptionOccured("Cancellation probability cannot be higher than 1");
				notValid=true;
			}
			//if not valid set the field to default
			if (notValid){
				setUpDefaultValues();
			}
			else{//if valid run the simulation
				gb = new GUIBackend(GUISimulator.seed, GUISimulator.queue, GUISimulator.mean,GUISimulator.sd, GUISimulator.cancel, GUISimulator.first, GUISimulator.business, GUISimulator.premium, GUISimulator.economy);
				
				btnRunSimulator.setEnabled(false);
				processSimulation();
				btnShowChart.setEnabled(true);
				displayChart("Progress",gb.getSimulationDataSet());
				//make text boxes not editable
				textFieldRNGSeed.setEditable(false);
				textFieldDailyMean.setEditable(false);
				textFieldQueueSize.setEditable(false);
				textFieldCancellation.setEditable(false);
				textFieldFirst.setEditable(false);
				textFieldBusiness.setEditable(false);
				textFieldPremium.setEditable(false);
				textFieldEconomy.setEditable(false);
			}
	}
	
	/**
	 * This function is responsible for simulating the flights 
	 * Above is called only if validation rule are not violated 
	 * 
	 * @throws AircraftException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws PassengerException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws SimulationException See methods from {@link asgn2Simulators.Simulator} 
	 * @throws IOException 

	 */
	private void processSimulation() throws AircraftException, SimulationException, PassengerException, IOException{
		gb.processFlights();
		String result = gb.getResults();
		textAreaSimulationResults.setText(result);
	}
	
	/**
	 * This function is responsible for generating the dialog box if inputs are not valid
	 * 
	 * @param msg <code>String</code> detail message of error
	 */
	private void exceptionOccured(String msg){
		JOptionPane.showMessageDialog(null, msg, "Invalid", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * This function is responsible for pushing the chart in JPanel after simulation is finished
	 * 
	 * @param title <code>String</code> title of the chart
	 * @param data <code>XYDataset</code> data set for displaying series
	 */
	private void displayChart(String title, XYDataset data){
		 chart = createChart(title,data);
		 chartpanel.setChart(chart);
	}
	
	/**
	 * This function is responsible for constructing the chart after simulation is finished
	 * 
	 * @param title <code>String</code> title of the chart
	 * @param data <code>XYDataset</code> data set for displaying series
	 * @return chart containing the plotted data
	 */
	private JFreeChart createChart(String title, XYDataset data){
		final JFreeChart c1 = ChartFactory.createXYLineChart(
	            title,      // chart title
	            "Days",                      // x axis label
	            "Passengers",                      // y axis label
	            data,                  // data
	            PlotOrientation.VERTICAL,
	            true,                     // include legend
	            true,                     // tooltips
	            false                     // urls
	        );
        c1.setBackgroundPaint(Color.white);
        final XYPlot plot = c1.getXYPlot();
        plot.setBackgroundPaint(Color.lightGray);
        plot.setDomainGridlinePaint(Color.white);
        plot.setRangeGridlinePaint(Color.white);
        return c1;
	}
	

}
