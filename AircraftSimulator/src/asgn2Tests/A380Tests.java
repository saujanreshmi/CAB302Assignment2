/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Test;

import asgn2Aircraft.A380;
import asgn2Aircraft.AircraftException;
import asgn2Passengers.Business;
import asgn2Passengers.Economy;
import asgn2Passengers.First;
import asgn2Passengers.Passenger;
import asgn2Passengers.PassengerException;
import asgn2Passengers.Premium;

/**
 * @author saujanthapa
 *
 */
public class A380Tests {

	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 * @throws AircraftException 
	 */
	@Test(expected=AircraftException.class)
	public void testA380StringIntEmptyFlightCode() throws AircraftException {
		A380 a0 = new A380("",1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int)}.
	 * @throws AircraftException 
	 */
	@Test(expected=AircraftException.class)
	public void testA380StringIntInvalidDepartureTime() throws AircraftException {
		A380 a1 = new A380("QF11",0);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 */
	@Test(expected=AircraftException.class)
	public void testA380StringIntIntIntIntIntInvalidfirst() throws AircraftException {
		A380 a2 = new A380("QF11",1,-1,0,0,0);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 */
	@Test(expected=AircraftException.class)
	public void testA380StringIntIntIntIntIntInvalidbusiness() throws AircraftException {
		A380 a3 = new A380("QF11",1,0,-1,0,0);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 */
	@Test(expected=AircraftException.class)
	public void testA380StringIntIntIntIntIntInvalidpremium() throws AircraftException {
		A380 a4 = new A380("QF11",1,0,0,-1,0);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.A380#A380(java.lang.String, int, int, int, int, int)}.
	 * @throws AircraftException 
	 */
	@Test(expected=AircraftException.class)
	public void testA380StringIntIntIntIntIntInvalideconomy() throws AircraftException {
		A380 a5 = new A380("QF11",1,0,0,0,-1);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=PassengerException.class)
	public void testCancelBookingInvalidCancellationTime() throws AircraftException, PassengerException {
		A380 a32=new A380("QF11",4,1,1,1,1);
		First f1= new First(0,4);
		a32.confirmBooking(f1, 1);
		a32.cancelBooking(f1, -1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testCancelBookingPassengerNotConfirmedFirst() throws AircraftException, PassengerException {
		A380 a31=new A380("QF11",4,1,1,1,1);
		First f1= new First(0,4);
		a31.cancelBooking(f1, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testCancelBookingPassengerNotConfirmedBusiness() throws AircraftException, PassengerException {
		A380 a31=new A380("QF11",4,1,1,1,1);
		Business b1= new Business(0,4);
		a31.cancelBooking(b1, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testCancelBookingPassengerNotConfirmedPremium() throws AircraftException, PassengerException {
		A380 a31=new A380("QF11",4,1,1,1,1);
		Premium p1= new Premium(0,4);
		a31.cancelBooking(p1, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#cancelBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testCancelBookingPassengerNotConfirmedEconomy() throws AircraftException, PassengerException {
		A380 a31=new A380("QF11",4,1,1,1,1);
		Economy e1= new Economy(0,4);
		a31.cancelBooking(e1, 1);
	}
	

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmBookingPassengerInvalidStateFlown() throws AircraftException, PassengerException {//this might have for forms for potential test
		//fail("Not yet implemented");
		A380 a26 = new A380("QF11",4,2,2,2,2);
		First f1 =new First(0,4);
		a26.confirmBooking(f1, 1);
		a26.flyPassengers(4);
		a26.confirmBooking(f1, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmBookingPassengerInvalidStateRefused() throws AircraftException, PassengerException {
		A380 a26 = new A380("QF11",4,2,2,2,2);
		First f1 =new First(0,4);
		f1.refusePassenger(2);
		a26.confirmBooking(f1, 1);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testConfirmBookingPassengerSeatsNotAvailableFirst() throws AircraftException, PassengerException {
		A380 a27 = new A380("QF11",4,1,2,2,2);
		First f1 =new First(0,4);
		First f2 = new First(0,4);
		a27.confirmBooking(f1, 1);
		a27.confirmBooking(f2, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testConfirmBookingPassengerSeatsNotAvailableBusiness() throws AircraftException, PassengerException {
		A380 a28 = new A380("QF11",4,2,1,2,2);
		Business b1 =new Business(0,4);
		Business b2 = new Business(0,4);
		a28.confirmBooking(b1, 1);
		a28.confirmBooking(b2, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testConfirmBookingPassengerSeatsNotAvailablePremium() throws AircraftException, PassengerException {
		A380 a29 = new A380("QF11",4,2,2,1,2);
		Premium p1 =new Premium(0,4);
		Premium p2 = new Premium(0,4);
		a29.confirmBooking(p1, 1);
		a29.confirmBooking(p2, 1);
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#confirmBooking(asgn2Passengers.Passenger, int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test(expected=AircraftException.class)
	public void testConfirmBookingPassengerSeatsNotAvailableEconomy() throws AircraftException, PassengerException {
		A380 a30 = new A380("QF11",4,2,2,2,1);
		Economy e1 =new Economy(0,4);
		Economy e2 = new Economy(0,4);
		a30.confirmBooking(e1, 1);
		a30.confirmBooking(e2, 1);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightEmpty()}.
	 * @throws AircraftException 
	 */
	@Test
	public void testFlightEmptyInital() throws AircraftException {
		//fail("Not yet implemented");
		A380 a8 = new A380("QF11",1,0,0,0,0);
		assertTrue(a8.flightEmpty());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightEmpty()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testFlightEmptyPassengerRemoved() throws AircraftException, PassengerException {
		A380 a11 = new A380 ("QF11",2,2,0,0,0);
		First f= new First(0,2);
		a11.confirmBooking(f, 0);
		a11.cancelBooking(f, 1);
		assertTrue(a11.flightEmpty());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightFull()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testFlightFullTrue() throws AircraftException, PassengerException {
		A380 a10 = new A380("QF11",3,1,1,1,1);
		First f1 = new First(0,3);
		Business b1 = new Business(0,3);
		Premium p1 = new Premium(0,3);
		Economy e1 = new Economy(0,3);
		a10.confirmBooking(f1, 1);
		a10.confirmBooking(b1, 1);
		a10.confirmBooking(p1, 1);
		a10.confirmBooking(e1, 1);
		assertTrue(a10.flightFull());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flightFull()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testFlightFullFalse() throws AircraftException, PassengerException {
		A380 a11 = new A380("QF11",3,1,1,1,1);
		First f1 = new First(0,3);
		Business b1 = new Business(0,3);
		Premium p1 = new Premium(0,3);
		a11.confirmBooking(f1, 1);
		a11.confirmBooking(b1, 1);
		a11.confirmBooking(p1, 1);
		assertFalse(a11.flightFull());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flyPassengers(int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testFlyPassengersTrue() throws AircraftException, PassengerException {
		A380 a12 = new A380("QF11",4,1,1,1,1);
		First f1 = new First(0,4);
		a12.confirmBooking(f1, 1);
		a12.flyPassengers(5);
		assertTrue(f1.isFlown());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#flyPassengers(int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testFlyPassengersFalse() throws AircraftException, PassengerException {
		A380 a13 = new A380("QF11",4,1,1,1,1);
		First f1 = new First(0,4);
		a13.confirmBooking(f1, 1);
		a13.cancelBooking(f1, 2);
		a13.flyPassengers(5);
		assertFalse(f1.isFlown());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetBookings() throws AircraftException, PassengerException {
		A380 a14 = new A380("QF11",4,1,1,1,1);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		a14.confirmBooking(f1, 1);
		a14.confirmBooking(b1, 1);
		assertEquals(2,a14.getBookings().getAvailable());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumBusiness()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetNumBusiness() throws AircraftException, PassengerException {
		A380 a15 = new A380("QF11",4,1,2,1,1);
		Business b1 = new Business(0,4);
		a15.confirmBooking(b1, 1);
		assertEquals(1,a15.getNumBusiness());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumEconomy()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetNumEconomy() throws AircraftException, PassengerException {
		A380 a16 = new A380("QF11",4,1,1,1,2);
		Economy e1 = new Economy(0,4);
		a16.confirmBooking(e1, 1);
		assertEquals(1,a16.getNumEconomy());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumFirst()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetNumFirst() throws AircraftException, PassengerException {
		A380 a17 = new A380("QF11",4,1,1,1,1);
		First f1 = new First(0,4);
		a17.confirmBooking(f1, 1);
		assertEquals(1,a17.getNumFirst());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumPassengers()}.
	 * @throws AircraftException 
	 */
	@Test
	public void testGetNumPassengersNonConfirmed() throws AircraftException {
		A380 a18 = new A380("QF11",4,1,1,1,1);
		assertEquals(0,a18.getNumPassengers());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumPassengers()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetNumPassengersSomeConfirmed() throws AircraftException, PassengerException {
		A380 a18 = new A380("QF11",4,1,1,1,1);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		a18.confirmBooking(f1, 1);
		a18.confirmBooking(b1, 1);
		assertEquals(2,a18.getNumPassengers());
	}
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getNumPremium()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetNumPremium() throws AircraftException, PassengerException {
		A380 a19 = new A380("QF11",4,1,1,1,1);
		Premium p1 = new Premium(0,4);
		a19.confirmBooking(p1, 1);
		assertEquals(1,a19.getNumPremium());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getPassengers()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetPassengers() throws AircraftException, PassengerException {
		A380 a20 = new A380("QF11",4,1,1,1,1);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		Premium p1 = new Premium(0,4);
		a20.confirmBooking(f1, 1);
		a20.confirmBooking(b1, 1);
		List<Passenger> s = a20.getPassengers();
		a20.confirmBooking(p1, 1);
		assertEquals("Does not make the deep copy",2,s.size());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#getStatus(int)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testGetStatus() throws AircraftException, PassengerException {
		A380 a21 = new A380("QF11",4,2,2,2,2);
		String str1 = "2::3::F:1::J:1::P:0::Y:1|F:N/Q>C||J:N/Q>C||Y:N/Q>C|\n";
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		Economy e1 = new Economy(0,4);
		a21.confirmBooking(f1, 1);
		a21.confirmBooking(b1, 1);
		a21.confirmBooking(e1, 1);
		String str2 = a21.getStatus(2);
		assertEquals("Does not match",str1,str2);
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#hasPassenger(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testHasPassenger() throws AircraftException, PassengerException {
		A380 a22 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		a22.confirmBooking(f1, 1);
		assertTrue("Does not exists this passenger",a22.hasPassenger(f1));
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#initialState()}.
	 * @throws AircraftException 
	 */
	@Test
	public void testInitialState() throws AircraftException {
		A380 a23 = new A380("QF11",4,2,2,2,2);
		String str = "A380:QF11:4 Capacity: 8 [F: 2 J: 2 P: 2 Y: 2]";
		assertEquals("Does not match",str,a23.initialState());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testSeatsAvailableInitial() throws AircraftException, PassengerException {
		A380 a24 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		assertTrue("Seats not available",a24.seatsAvailable(f1));
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#seatsAvailable(asgn2Passengers.Passenger)}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testSeatsAvailableSomeFilled() throws AircraftException, PassengerException {
		A380 a25 = new A380("QF11",4,2,2,2,2);
		Business b1 = new Business(0,4);
		a25.confirmBooking(b1, 1);
		assertTrue("Seats not available",a25.seatsAvailable(b1));
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#toString()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testToString() throws AircraftException, PassengerException {
		A380 a24 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		Economy e1 = new Economy(0,4);
		a24.confirmBooking(f1, 1);
		a24.confirmBooking(b1, 1);
		a24.confirmBooking(e1, 1);
		String expected="A380:QF11:4 Count: 3 [F: 1 J: 1 P: 0 Y: 1]";
		assertEquals("String did not match",expected,a24.toString());
	}

	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsFirst() throws AircraftException, PassengerException {
		A380 a32 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		a32.confirmBooking(f1, 1);
		a32.upgradeBookings();
		assertEquals("Able to upgrade from first",1,a32.getNumFirst());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsBusinessCountFirst() throws AircraftException, PassengerException {
		A380 a32 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		a32.confirmBooking(f1, 1);
		a32.confirmBooking(b1, 1);
		a32.upgradeBookings();
		assertEquals("Able to upgrade from first",2,a32.getNumFirst());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsBusinessCountBusiness() throws AircraftException, PassengerException {
		A380 a32 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		a32.confirmBooking(f1, 1);
		a32.confirmBooking(b1, 1);
		a32.upgradeBookings();
		assertEquals("Able to upgrade from first",0,a32.getNumBusiness());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsPremiumCountBusiness() throws AircraftException, PassengerException {
		A380 a33 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		First f2 = new First(0,4);
		Business b1 = new Business(0,4);
		Premium p1 = new Premium(0,4);
		a33.confirmBooking(f1, 1);
		a33.confirmBooking(f2, 1);
		a33.confirmBooking(b1, 1);
		a33.confirmBooking(p1,1);
		a33.upgradeBookings();
		assertEquals("Able to upgrade from first",2,a33.getNumBusiness());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsPremiumCountPremium() throws AircraftException, PassengerException {
		A380 a33 = new A380("QF11",4,2,2,2,2);
		First f1 = new First(0,4);
		First f2 = new First(0,4);
		Business b1 = new Business(0,4);
		Premium p1 = new Premium(0,4);
		a33.confirmBooking(f1, 1);
		a33.confirmBooking(f2, 1);
		a33.confirmBooking(b1, 1);
		a33.confirmBooking(p1,1);
		a33.upgradeBookings();
		assertEquals("Able to upgrade from first",0,a33.getNumPremium());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsEconomyCountPremium() throws AircraftException, PassengerException {
		A380 a34 = new A380("QF11",4,1,1,1,2);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		Economy e1 = new Economy(0,4);
		Economy e2 = new Economy(0,4);
		a34.confirmBooking(f1, 1);
		a34.confirmBooking(b1,1);
		a34.confirmBooking(e1,1);
		a34.confirmBooking(e2,1);
		a34.upgradeBookings();
		assertEquals("Able to upgrade from first",1,a34.getNumPremium());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsEconomyCountEconomy() throws AircraftException, PassengerException {
		A380 a34 = new A380("QF11",4,1,1,1,2);
		First f1 = new First(0,4);
		Business b1 = new Business(0,4);
		Economy e1 = new Economy(0,4);
		Economy e2 = new Economy(0,4);
		Economy e3 = new Economy(0,4);
		a34.confirmBooking(f1, 1);
		a34.confirmBooking(b1,1);
		a34.confirmBooking(e1,1);
		a34.confirmBooking(e2,1);
		a34.upgradeBookings();
		a34.confirmBooking(e3, 1);
		assertEquals("Able to upgrade from first",2,a34.getNumEconomy());
	}
	
	/**
	 * Test method for {@link asgn2Aircraft.Aircraft#upgradeBookings()}.
	 * @throws AircraftException 
	 * @throws PassengerException 
	 */
	@Test
	public void testUpgradeBookingsBusinessCountEconomy() throws AircraftException, PassengerException {
		A380 a34 = new A380("QF11",4,2,2,3,3);
		Business b1 = new Business(0,4);
		Economy e1 = new Economy(0,4);
		Economy e2 = new Economy(0,4);
		Economy e3 = new Economy(0,4);
		a34.confirmBooking(b1,1);
		a34.confirmBooking(e1,1);
		a34.confirmBooking(e2,1);
		a34.confirmBooking(e3,1);
		a34.upgradeBookings();
		assertEquals("Able to upgrade from economy to business",0,a34.getNumBusiness());
	}
	
}
