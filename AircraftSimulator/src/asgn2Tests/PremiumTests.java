/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.*;


/**
 * @author n8962146
 *
 */
public class PremiumTests {
	
	/**
	 * Test method for {@link asgn2Passengers.Premium#upgrade()}.
	 * Tests if Premium upgrade method works as intended
	 */
	@Test
	public void testUpgradeWorks() throws PassengerException{
		Passenger p = new Premium(1,2);
		p = p.upgrade();
		assertTrue(p instanceof Business);		
	}
	
}
