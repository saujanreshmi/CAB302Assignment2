/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.*;

/**
 * @author n8962146
 *
 */
public class EconomyTests {
	
	/**
	 * Test method for {@link asgn2Passengers.Economy#upgrade()}.
	 * Tests if Economy upgrade method works as intended
	 */
	@Test
	public void testUpgradeWorks() throws PassengerException{
		Passenger p = new Economy(1, 2);
		//Premium p2 = p.upgrade();
		//p.upgrade();
		assertTrue(p.upgrade() instanceof Premium);
	}
	
}
