/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import asgn2Passengers.*;


/**
 * @author n8962146
 *
 */
public class BusinessTests {
			
	/**
	 * Test method for {@link asgn2Passengers.Business#upgrade()}.
	 * Tests if Business upgrade method works as intended
	 */
	@Test
	public void testUpgradeWorks() throws PassengerException{
		Passenger p = new Business(1,2);
		p = p.upgrade();
		assertTrue(p instanceof First);	
	}
		
}
