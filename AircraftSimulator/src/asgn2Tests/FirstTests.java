/**
 * 
 */
package asgn2Tests;

import static org.junit.Assert.*;

import org.junit.*;

import asgn2Passengers.*;


/**
 * @author n8962146
 *
 */
public class FirstTests {
		
	/**
	 * Test method for {@link asgn2Passengers.First#First(int, int)}.
	 * Test for Booking Time < 0 
	 */
	@Test(expected=PassengerException.class)
	public void testBookingTimeInRangeNegative() throws PassengerException{
		Passenger p = new First(-1, 1);
	}	

	/**
	 * Test method for {@link asgn2Passengers.First#First(int, int)}.
	 * Test for Departure Time <= 0
	 */
	@Test(expected=PassengerException.class)
	public void testDepartureTimeInRange0() throws PassengerException{
		Passenger p = new First(0, 0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.First#First(int, int)}.
	 * Test for Departure TIme < Booking Time
	 */
	@Test(expected=PassengerException.class)
	public void testDepartureTimeLessThanBookingTime() throws PassengerException{
		Passenger p = new First(2, 1);
	}

	
	/**
	 * Test method for {@link asgn2Passengers.First#upgrade()}.
	 * Tests if First upgrade function works as intended
	 */
	@Test
	public void testUpgradeWorks() throws PassengerException{
		Passenger p = new First(1, 2);
		p = p.upgrade();		
		assertTrue(p instanceof First);	
	}	
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether passenger is New
	 */
	@Test(expected=PassengerException.class)
	public void testCancelSeatIsNew() throws PassengerException {
		Passenger p = new First(0, 1);
		p.cancelSeat(0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether passenger is In Queue
	 */
	@Test(expected=PassengerException.class)
	public void testCancelSeatIsInQueue() throws PassengerException {
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.cancelSeat(0);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether passenger is Refused
	 */
	@Test(expected=PassengerException.class)
	public void testCancelSeatIsRefused() throws PassengerException {
		Passenger p = new First(0, 1);
		p.refusePassenger(0);
		p.cancelSeat(0);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether passenger is Flown
	 */
	@Test(expected=PassengerException.class)
	public void testCancelSeatIsFlown() throws PassengerException {
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.flyPassenger(0);
		p.cancelSeat(0);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether passenger Departure Time <= 0
	 */
	@Test(expected=PassengerException.class)
	public void testCancelSeatIsInRange0() throws PassengerException {
		Passenger p = new First(0, 0);
		p.cancelSeat(0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether passenger Departure Time < Cancellation Time
	 */
	@Test(expected=PassengerException.class)
	public void testCancelSeatDepartureTimeLessThanCancellationTime() throws PassengerException {
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.cancelSeat(2);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#cancelSeat(int)}.
	 * Test whether cancelSeat() works as intended
	 */
	@Test
	public void testCancelSeatWorks() throws PassengerException {
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.cancelSeat(0);
		assertTrue(!p.isConfirmed() && p.isNew() && p.getBookingTime() == 0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if passenger is already confirmed
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmSeatIsConfirmed() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.confirmSeat(0, 1);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if Passenger has been refused
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmSeatIsRefused() throws PassengerException{
		Passenger p = new First(0, 1);
		p.refusePassenger(0);
		p.confirmSeat(0, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if passenger has flown
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmSeatIsFlown() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.flyPassenger(0);
		p.confirmSeat(0, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if Confirmation Time < 0
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmSeatInRangeNegative() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(-1, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if Departure Time < Confirmation Time
	 */
	@Test(expected=PassengerException.class)
	public void testConfirmSeatDepartureTimeLessThanConfirmationTime() throws PassengerException{
		Passenger p = new First(2, 3);
		p.confirmSeat(2, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if confirmSeat works as intended for a new passenger
	 */
	@Test
	public void testConfirmSeatWorksIsNew() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		assertTrue(p.isConfirmed() && p.getConfirmationTime() == 0 && p.getDepartureTime() == 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#confirmSeat(int, int)}.
	 * Tests if confirmSeat works as intended for a queued passenger
	 */
	@Test
	public void testConfirmSeatWorksIsQueued() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.confirmSeat(0, 1);
		assertTrue(p.isConfirmed() && p.getConfirmationTime() == 0 && p.getDepartureTime() == 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * Tests if passenger is new
	 */
	@Test(expected=PassengerException.class)
	public void testFlyPassengerIsNew() throws PassengerException {
		Passenger p = new First(0, 1);
		p.flyPassenger(1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * Tests if passenger is in Queue
	 */
	@Test(expected=PassengerException.class)
	public void testFlyPassengerIsInQueue() throws PassengerException {
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.flyPassenger(1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * Tests if passenger is refused
	 */
	@Test(expected=PassengerException.class)
	public void testFlyPassengerIsRefused() throws PassengerException {
		Passenger p = new First(0, 1);
		p.refusePassenger(0);
		p.flyPassenger(1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * Tests if passenger is flown
	 */
	@Test(expected=PassengerException.class)
	public void testFlyPassengerIsFlown() throws PassengerException {
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.flyPassenger(1);
		p.flyPassenger(1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * Tests if Departure Time <= 0
	 */
	@Test(expected=PassengerException.class)
	public void testFlyPassengerInRange0() throws PassengerException {
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.flyPassenger(0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#flyPassenger(int)}.
	 * Tests if flyPassenger works as intended
	 */
	@Test
	public void testFlyPassengerWorks() throws PassengerException {
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.flyPassenger(1);
		assertTrue(!p.isConfirmed() && p.isFlown() && p.getDepartureTime() == 1);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * Tests if passenger is in queue
	 */
	@Test(expected=PassengerException.class)
	public void testQueuePassengerInQueue() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.queuePassenger(0, 1);
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * Tests if passenger is confirmed
	 */
	@Test(expected=PassengerException.class)
	public void testQueuePassengerIsConfirmed() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.queuePassenger(0, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * Tests if passenger is refused
	 */
	@Test(expected=PassengerException.class)
	public void testQueuePassengerIsRefused() throws PassengerException{
		Passenger p = new First(0, 1);
		p.refusePassenger(0);
		p.queuePassenger(0, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * Tests if in Queue Time is < 0
	 */
	@Test(expected=PassengerException.class)
	public void testQueuePassengerInRangeNegative() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(-1, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * Tests if Departure Time < Queue Time
	 */
	@Test(expected=PassengerException.class)
	public void testQueuePassengerDepartureTimeLessThanQueueTime() throws PassengerException{
		Passenger p = new First(2, 3);
		p.queuePassenger(2, 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#queuePassenger(int, int)}.
	 * Tests if queuePassenger works as intended
	 */
	@Test
	public void testQueuePassengerWorks() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		assertTrue(!p.isNew() && p.isQueued() && p.getEnterQueueTime() == 0 && p.getDepartureTime() == 1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * Tests if passenger is confirmed
	 */
	@Test(expected=PassengerException.class)
	public void testRefusePassengerIsConfirmed() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.refusePassenger(0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * Tests if passenger is refused
	 */
	@Test(expected=PassengerException.class)
	public void testRefusePassengerIsRefused() throws PassengerException{
		Passenger p = new First(0, 1);
		p.refusePassenger(0);
		p.refusePassenger(0);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * Tests if passenger is flown
	 */
	@Test(expected=PassengerException.class)
	public void testRefusePassengerIsFlown() throws PassengerException{
		Passenger p = new First(0, 1);
		p.confirmSeat(0, 1);
		p.flyPassenger(1);
		p.refusePassenger(1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * Tests if Refusal Time is < 0
	 */
	@Test(expected=PassengerException.class)
	public void testRefusePassengerRefusalTimeNegative() throws PassengerException{
		Passenger p = new First(0, 1);
		p.refusePassenger(-1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 */
	@Test(expected=PassengerException.class)
	public void testRefusePassengerRefusalTimeLessThanBookingTime() throws PassengerException{
		Passenger p = new First(2, 3);
		p.refusePassenger(1);
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * Tests if refusePassenger method works as intended if the passenger is new
	 */
	@Test
	public void testRefusePassengerWorksIsNew() throws PassengerException{
		Passenger p = new First(0, 1);
		p.refusePassenger(0);
		assertTrue(p.isRefused());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#refusePassenger(int)}.
	 * Tests if refusePassenger method works as intended if the passenger is queued
	 */
	@Test
	public void testRefusePassengerWorksIsQueued() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.refusePassenger(0);
		assertTrue(p.isRefused());
	}
	
	/**
	 * Test method for {@link asgn2Passengers.Passenger#wasConfirmed()}.
	 * Tests if wasConfirmed works as intended
	 */
	@Test
	public void testWasConfirmedWorks() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.confirmSeat(0, 1);
		p.cancelSeat(0);
		assertTrue(p.wasConfirmed());
	}

	/**
	 * Test method for {@link asgn2Passengers.Passenger#wasQueued()}.
	 * Tests if wasQueued works as intended
	 */
	@Test
	public void testWasQueuedWorks() throws PassengerException{
		Passenger p = new First(0, 1);
		p.queuePassenger(0, 1);
		p.refusePassenger(0);
		assertTrue(p.wasQueued());
	}

}
